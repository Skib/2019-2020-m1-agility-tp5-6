// Test d'exemple par défaut :
describe('The Traffic web site home page', () => {
    it('successfully loaded', () => {
        cy.visit('/')
    })
})

describe('Story 1 -> Configuration page', () => {
    it('successfully arrived on configuration page', () =>{
        cy.get('a[href="#configuration"]').click()
        cy.url().should('include', '#configuration')
    })
})


describe('Story 2 -> 28 segments', () => {
    it('successfully counted 28 segments', () => {
        cy.get('form[data-kind="segment"]').should('have.length', 28)
    })
})

describe('Story 3 -> 0 vehicle', () => {
    it('successfully counted 0 vehicle', () => {
        cy.get('form[data-kind="vehicle"]').should('have.length', 1)
    })
})

describe('Story 4 -> Modify segment 5', () => {
    it('successfully changed segment 5 speed to 30', () => {
        // clears and types the wanted value in the good input
        cy.get('input[name="speed"][form="segment-5"]').clear().type('30')
        cy.get('form[id="segment-5"]').click()
        // clicks on Close button
        cy.get('.modal-footer > .btn').click()

        // get the elements on server side
        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response)=>{
            expect(response.body['segments']['4']).to.have.property('speed', 30)
        })
    })
})

describe('Story 5 -> Modify roundabout', () => {
    it('successfully changed duration and capacity of roundabout', () => {
        cy.get('input[name="capacity"]').clear().type('4')
        cy.get('input[name="duration"]').clear().type('15')
        cy.get('form[data-kind="roundabout"]').children('button').click()

        cy.reload()

        cy.get('a[href="#configuration"]').click()

        cy.get('input[name="capacity"]').should('have.value',4)
        cy.get('input[name="duration"]').should('have.value',15)

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response)=>{
            expect(response.body['crossroads']['2']).to.have.property('capacity', 4)
            expect(response.body['crossroads']['2']).to.have.property('duration', 15)
        })
    })
})

describe('Story 6 -> Modify TrafficLight #29', () => {
    it('successfully changed trafficlight values', () => {
        cy.get('input[name="orangeDuration"][form="trafficlight-29"]').clear().type('4')
        cy.get('input[name="greenDuration"][form="trafficlight-29"]').clear().type('40')
        cy.get('input[name="nextPassageDuration"][form="trafficlight-29"]').clear().type('8')
        cy.get('form[data-kind="trafficlight"][name="trafficlight-29"]').children('button').click()
        cy.get('.modal-footer > .btn').click()

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response)=>{
            expect(response.body['crossroads']['0']).to.have.property('orangeDuration', 4)
            expect(response.body['crossroads']['0']).to.have.property('greenDuration', 40)
            expect(response.body['crossroads']['0']).to.have.property('nextPassageDuration', 8)
        })
    })
})

describe('Story 7 -> Add 3 vehicles', () => {
    it('successfully added 3 vehicles and checked them', () => {
        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('50')
        cy.get('form[name="addVehicle"').children('button').click()
        cy.get('.modal-footer').children('button').click()

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('19')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('8')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('200')
        cy.get('form[name="addVehicle"').children('button').click()
        cy.get('.modal-footer').children('button').click()

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('27')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('2')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('150')
        cy.get('form[name="addVehicle"').children('button').click()
        cy.get('.modal-footer').children('button').click()
        

        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response)=>{
            expect(response.body['200.0']['0']).to.have.property('speed', 0)
            expect(response.body['150.0']['0']).to.have.property('speed', 0)
            expect(response.body['50.0']['0']).to.have.property('speed', 0)
        })
        
        // checks the speed values of the 3 vehicles on configuration page
        cy.get('.col-md-8 > .table > tbody > :nth-child(1) > :nth-child(3)').contains('0')
        cy.get('.col-md-8 > .table > tbody > :nth-child(2) > :nth-child(3)').contains('0')
        cy.get('.col-md-8 > .table > tbody > :nth-child(3) > :nth-child(3)').contains('0')
    })
})


describe('Story 8 -> 120 seconds simulation', () => {
    it('successfully simulated and checked moving vehicles', () => {
        cy.get('a[href="#simulation"]').click()
        cy.get('input[name="time"]').clear().type('120')
        cy.get('button').contains('Run').click()
        cy.wait(15000)

        // checks the progress bar value
        cy.get('.progress-bar').should('have.attr','aria-valuenow',100)

        // checks the moving values of the 3 vehicles on simulation page
        cy.get('.col-md-7 > .table > tbody > :nth-child(1) > :nth-child(6)').contains('block')
        cy.get('.col-md-7 > .table > tbody > :nth-child(2) > :nth-child(6)').contains('block')
        cy.get('.col-md-7 > .table > tbody > :nth-child(3) > :nth-child(6)').contains('play_circle_filled')
    })
})


describe('Story 9 -> 500 seconds simultion', () => {
    it('successfully simulated and checked moving vehicles', () => {
        cy.reload()
        cy.get('a[href="#configuration"]').click()
        
        cy.get('td').contains('No vehicle available')

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('50')
        cy.get('form[name="addVehicle"').children('button').click()
        cy.get('.modal-footer').children('button').click()

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('19')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('8')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('200')
        cy.get('form[name="addVehicle"').children('button').click()
        cy.get('.modal-footer').children('button').click()

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('27')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('2')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('150')
        cy.get('form[name="addVehicle"').children('button').click()
        cy.get('.modal-footer').children('button').click()

        cy.get('a[href="#simulation"]').click()

        cy.get('input[name="time"]').clear().type('500')
        cy.get('button').contains('Run').click()

        cy.wait(60000)
        cy.get('.progress-bar').should('have.attr','aria-valuenow',100)

        cy.get('.col-md-7 > .table > tbody > :nth-child(1) > :nth-child(6)').contains('block')
        cy.get('.col-md-7 > .table > tbody > :nth-child(2) > :nth-child(6)').contains('block')
        cy.get('.col-md-7 > .table > tbody > :nth-child(3) > :nth-child(6)').contains('block')
    })
})

describe('Story 10 -> 200 seconds simulation', () => {
    it('successfully simulated and checked vehicles on good segments', () => {
        cy.reload()

        cy.get('a[href="#configuration"]').click()

        cy.get('td').contains('No vehicle available')
        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('50')
        cy.get('form[name="addVehicle"').children('button').click()
        cy.get('.modal-footer').children('button').click()

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('80')
        cy.get('form[name="addVehicle"').children('button').click()
        cy.get('.modal-footer').children('button').click()

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5')
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26')
        cy.get('input[name="time"][form="addVehicle"]').clear().type('80')
        cy.get('form[name="addVehicle"').children('button').click()
        cy.get('.modal-footer').children('button').click()

        cy.get('a[href="#simulation"]').click()

        cy.get('input[name="time"]').clear().type('200')
        cy.get('button').contains('Run').click()
        cy.wait(25000)
        cy.get('.progress-bar').should('have.attr','aria-valuenow',100)

        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response)=>{
            expect(response.body['50.0']['0']['step']).to.have.property('id', 17)
            expect(response.body['80.0']['0']['step']).to.have.property('id', 29)
            expect(response.body['80.0']['1']['step']).to.have.property('id', 29)
        })


    })
})

